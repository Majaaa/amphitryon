package com.example.ap_android_amphitryon.modeles.DTO;

public class Utilisateur {
    private String id;
    private String prenom;
    private String nom;
    private String login;
    private String statut;


    public Utilisateur() {}

    public Utilisateur(String id, String prenom, String nom, String login, String statut) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.login = login;
        this.statut = statut;
    }

    public String getId() {
        return this.id;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public String getLogin() {
        return this.login;
    }

    public String getStatut() {
        return this.statut;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
}
