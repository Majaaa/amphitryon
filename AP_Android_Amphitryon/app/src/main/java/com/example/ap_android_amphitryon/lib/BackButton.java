package com.example.ap_android_amphitryon.lib;

import android.app.Activity;
import android.content.Intent;

import com.example.ap_android_amphitryon.modeles.Param;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class BackButton {

    /**
     * Méthode qui met un listener sur le boutton retour et permet la déconnexion d'un utilisateur.
     * @param backButton
     * @param currentActivity
     * @param nextActivity
     */
    public static void listenerOnBackButton(FloatingActionButton backButton, Activity currentActivity, Class<?> nextActivity) {
        // Listener sur le bouton.
        backButton.setOnClickListener(v -> {
            // Si la vue a afficher est celle de la connexion, on supprime l'utilisateur
            // enregistrée dans le Param.
            if (nextActivity.getSimpleName().equals("MainActivity")) {
                Param.deleteUtilisateur();
            }

            // Intent vers la prochaine vue
            Intent intent = new Intent(currentActivity, nextActivity);
            currentActivity.startActivity(intent);
        });
    }


}
