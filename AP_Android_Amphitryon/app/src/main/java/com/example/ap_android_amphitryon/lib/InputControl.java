package com.example.ap_android_amphitryon.lib;

import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Tables;

import org.apache.commons.lang3.StringUtils;

public class InputControl {

    public static boolean controlNumTable(String numTable, int idService) {
        if (StringUtils.isNumeric(numTable)) {
            if (Integer.parseInt(numTable) >= 1 && Integer.parseInt(numTable) <= Tables.getMaxNumTable(idService) + 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean controlNbPlace(String nbPlace) {
        if (StringUtils.isNumeric(nbPlace) && Integer.parseInt(nbPlace) >= 2) {
            return true;
        }
        return false;
    }
}
