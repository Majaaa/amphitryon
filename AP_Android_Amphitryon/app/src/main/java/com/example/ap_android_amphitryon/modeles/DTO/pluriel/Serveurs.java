package com.example.ap_android_amphitryon.modeles.DTO.pluriel;

import com.example.ap_android_amphitryon.modeles.DAO.UtilisateurDAO;
import com.example.ap_android_amphitryon.modeles.DTO.Utilisateur;

import java.util.ArrayList;

public class Serveurs {

    /**
     * Attribut ArrayList<Utilisateur> contenant tous les Utilisateur de type SERVEUR uniquement.
     */
    private static ArrayList<Utilisateur> lesServeurs;


    /**
     * Méthode initialisant l'ArrayList<Utilisateur> lesServeurs.
     */
    private static void initialize() {
        lesServeurs = UtilisateurDAO.getServeurs();
    }


    /**
     * Getter de l'ArrayList lesServeurs.
     * @return ArrayList<Utilisateur>
     */
    public static ArrayList<Utilisateur> getServeurs() {
        if (lesServeurs == null) {
            initialize();
        }
        return lesServeurs;
    }


    /**
     * Méthode qui retourne une ArrayList<String> contenant la concatenation du
     * prénom et du nom des serveurs.
     * return ArrayList<String>
     */
    public static ArrayList<String> getServeursNames() {
        ArrayList<String> serveursNames = new ArrayList<String>();

        if (lesServeurs == null) {
            initialize();
        }

        for (Utilisateur serveur: lesServeurs) {
            serveursNames.add(serveur.getPrenom() + " " + serveur.getNom());
        }

        return serveursNames;
    }


    /**
     * Méthode qui retourne un object Utilisateur (Serveur) grace à son id.
     * @param idUser int
     */
    public static Utilisateur getServeurById(int idUser) {
        if (lesServeurs == null) {
            initialize();
        }

        for (Utilisateur serveur: lesServeurs) {
            if (Integer.parseInt(serveur.getId()) == idUser) {
                return serveur;
            }
        }
        return null;
    }
}