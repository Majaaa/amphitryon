package com.example.ap_android_amphitryon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ap_android_amphitryon.modeles.DAO.UtilisateurDAO;
import com.example.ap_android_amphitryon.modeles.Param;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Déclaration du TextViewError.
        final TextView textViewError = findViewById(R.id.textViewError);

        // Déclartion du bouton & méthode onClick du bouton Valider.
        final Button buttonValider = findViewById(R.id.buttonValider);
        buttonValider.setOnClickListener(v -> {
            // Récupère les editTexts
            final EditText editLogin = findViewById(R.id.inputLogin);
            final EditText editPassword = findViewById(R.id.inputMdp);

            // Récupère le contenue des editTexts
            final String login = editLogin.getText().toString();
            final String password = editPassword.getText().toString();

            // Si le couple login/mdp est correct.
            if (UtilisateurDAO.authentification(login, password)) {
                switchView();
            } else {
                textViewError.setText("Login ou mot de passe incorrect.");
            }
        });
    }

    /**
     * Méthode qui change de vue.
     */
    private void switchView () {
        Intent intent;
        switch (Param.getCurrentUtilisateur().getStatut()){
            case "ChefDeSalle":
                 intent = new Intent(MainActivity.this, chef_salle.class);
                break;
            case "ChefDeCuisine":
                intent = new Intent(MainActivity.this, chef_cuisinier.class);
                break;
            case "Serveur":
                intent = new Intent(MainActivity.this, serveurs.class);
                break;
            default:
                System.out.println("Statut inexistant.");
                intent = new Intent(MainActivity.this, MainActivity.class);

        }
        startActivity(intent);
    }




}