package com.example.ap_android_amphitryon.lib;

import android.app.Activity;
import android.content.Intent;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.ap_android_amphitryon.modeles.DTO.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GenerateXML {


    /**
     * Méthode qui créer des TextViews dans un LinearLayout
     * @param tables        ArrayList contenant les objets Tables
     * @param layout        Layout dans lequel afficher les tables
     * @return HashMap      Associe un objet à son textView
     */
    public static HashMap<Integer,Object> generateTextViews(ArrayList<Table> tables, LinearLayout layout){
        // HashMap qui associe un id textView à un objet Table.
        HashMap<Integer, Object> tablesAndIds = new HashMap<>();

        // Parcours de tous les objets Table de l'ArrayList.
        for (Table table : tables) {
            /** textView numéro table */
            // Créer le textView principal (Table n°), génère un id unique et associe l'id à ce textView.
            TextView textViewTable = new TextView(layout.getContext());
            int idTextViewTable = View.generateViewId();
            textViewTable.setId(idTextViewTable);

            // Ajoute le texte au TextView.
            textViewTable.setText(table.getLabel());

            // Créer les paramètres de mise en page pour le TextView.
            LinearLayout.LayoutParams layoutParamsTable = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            layoutParamsTable.setMargins(20, 30, 0, 0);

            // Met la taille du texte et les marges au textView.
            textViewTable.setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
            textViewTable.setLayoutParams(layoutParamsTable);

            // Ajout du TextView au LinearLayout.
            layout.addView(textViewTable);

            // Associe l'id du textView à l'objet
            tablesAndIds.put(idTextViewTable, table);

            /** textView info table */
            // Créer le textView contenant les infos d'une table, génère un id unique et associe l'id à ce textView.
            TextView textViewInfo = new TextView(layout.getContext());
            int idTextViewInfo = View.generateViewId();
            textViewInfo.setId(idTextViewInfo);

            // Ajoute le texte au TextView.
            textViewInfo.setText(table.getInfos());

            // Créer les paramètres de mise en page pour le TextView.
            LinearLayout.LayoutParams layoutParamsInfo = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            layoutParamsInfo.setMargins(60, 0, 0, 0);

            // Met la taille du texte et les marges au textView.
            textViewInfo.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textViewInfo.setLayoutParams(layoutParamsInfo);

            // Ajout du TextView au LinearLayout.
            layout.addView(textViewInfo);
        }

        // Retourne l'HashMap qui associe un TextView à son id.
        return tablesAndIds;
    }



    /**
     * Méthode qui parcours une liste d'objet XML et qui leur ajoute un listener au clique pour changer d'intent
     * en envoyant l'objet auquel ils sont associer
     * @param hashMapId    (Clé : Des id XML / Value : Des objet à envoyer en putExtra)
     * @param activity     (L'activity actuel)
     * @param nextActivity (L'acticity sur laquel on va se déplacer)
     */
    public static void onClickChangeIntent(HashMap<Integer,Object> hashMapId, Activity activity, Class<?> nextActivity){
        // Parcour le HashMap qui associe les id XML à leur Objet
        for (Map.Entry<Integer, Object> entry : hashMapId.entrySet()) {
            // Ajouter un événement au clique
            activity.findViewById(entry.getKey()).setOnClickListener(v -> {
                // Créer l'intent pour passer d'une activity à l'autre
                Intent intent = new Intent(activity, nextActivity);
                // Fait passer l'objet lors du changement d'activity
                intent.putExtra("extra", (Serializable) entry.getValue());
                // Lance le changement d'activity
                activity.startActivity(intent);
            });
        }
    }



}
