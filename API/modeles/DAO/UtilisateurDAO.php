<?php

class UtilisateurDAO {

	/**
	 * Méthode retournant les infos de l'utilisateur qui tente de se connecter.
	 * @param login
	 * @param mdp
	 * @return sql
	 */
	public static function authentification($login , $mdp){
		try{
			$prepSql = "SELECT id, nom, prenom, statut
						FROM utilisateur
						WHERE login = :login
						AND mdp = :mdp";

			$sql = DBConnex::getInstance()->prepare($prepSql);

			$sql->bindParam("login", $login);
			$sql->bindParam("mdp", $mdp);
			$sql->execute();

			return $sql->fetch(PDO::FETCH_ASSOC);
		} catch(Exception $e) {}

		return "Failed.";
	}


	/**
	 * Méthode retournant tous les utilisateurs dont le statut est `Serveur`.
	 * @return sql
	 */
	public static function getServeurs() {
		try{
			$prepSql = "SELECT id, nom, prenom, login, statut
						FROM utilisateur
						WHERE statut = 'Serveur';";

			$sql = DBConnex::getInstance()->prepare($prepSql);

			$sql->execute();

			return $sql->fetchAll(PDO::FETCH_ASSOC);
		} catch(Exception $e) {}

		return "Failed.";
	}

}

?>