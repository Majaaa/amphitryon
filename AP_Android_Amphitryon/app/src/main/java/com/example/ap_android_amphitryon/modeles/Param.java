package com.example.ap_android_amphitryon.modeles;

import com.example.ap_android_amphitryon.modeles.DTO.Utilisateur;

public class Param {

    /**
     * L'utilisateur actuellement connecté à l'application est stocké ici.
     * Si l'utilisateur n'a pas encoré été initialisé, il vaut null.
     */
    public static Utilisateur currentUtilisateur = null;

    /**
     * Getter de currentUtilisateur
     * @return Utilisateur
     */
    public static Utilisateur getCurrentUtilisateur() {
        return currentUtilisateur;
    }

    /**
     * Setter qui initialise currentUtilisateur
     * @param newUtilisateur
     */
    public static void setCurrentUtilisateur(Utilisateur newUtilisateur) {
        currentUtilisateur = newUtilisateur;
    }

    /**
     * Méthode qui met à null l'utilisateur connectée.
     */
    public static void deleteUtilisateur() {
        currentUtilisateur = null;
    }

}