package com.example.ap_android_amphitryon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ap_android_amphitryon.lib.BackButton;
import com.example.ap_android_amphitryon.lib.InputControl;
import com.example.ap_android_amphitryon.modeles.DAO.TableDAO;
import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Serveurs;
import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Tables;
import com.example.ap_android_amphitryon.modeles.DTO.Table;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class ModifTable extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif_table);

        // Déclaration du boutton retour, et met un listener dessus.
        FloatingActionButton backButton = findViewById(R.id.backButton);
        BackButton.listenerOnBackButton(backButton, ModifTable.this, chef_salle.class);

        // Déclaration du boutton Sauvegarder et du bouton Supprimer.
        final Button buttonSauvegarder = findViewById(R.id.buttonSauvegarder);
        final Button buttonSupprimer = findViewById(R.id.buttonSupprimer);

        // Récupère l'objet Table passé en extra de l'intent.
        Table targetedTable = (Table) getIntent().getSerializableExtra("extra");

        // Déclaration des editText, textViews et spinner
        EditText editTextNbPlace = findViewById(R.id.editTextNbPlace);
        TextView textViewNumTable = findViewById(R.id.textViewNumTable);
        TextView textViewErreur = findViewById(R.id.textViewErreur);
        Spinner spinnerServeurAffectee = findViewById(R.id.spinnerServeurAffectee);

        // Récupère l'idService.
        int idService = targetedTable.getIdService();

        // Remplies les champs du formulaire avec les infos de la tables.
        fillFields(targetedTable, textViewNumTable, editTextNbPlace, spinnerServeurAffectee);


        /** Listener du boutton Sauvegarder. */
        buttonSauvegarder.setOnClickListener(v -> {
            // Récupère la valeur du champ nbPlace
            String newNbPlaceControl = editTextNbPlace.getText().toString();

            // Controle de saisie du champ nbPlace.
            if (InputControl.controlNbPlace(newNbPlaceControl)) {
                // Attribut les nouvelles valeurs a l'objet Table.
                targetedTable.setNbPlace(Integer.parseInt(newNbPlaceControl));

                System.out.println(spinnerServeurAffectee.getSelectedItemId() + "----------------------");

                // Vérifie si un serveur a été affectée à la table (!= "Pas d'affectation").
                if (spinnerServeurAffectee.getSelectedItemId() != 0) {
                    targetedTable.setIdUser(Integer.parseInt(Serveurs.getServeurs().get((int) spinnerServeurAffectee.getSelectedItemId() - 1).getId()));
                } else {
                    targetedTable.setIdUser(0);
                }

                // Envoie de la requête à l'API.
                TableDAO.editTable(targetedTable);

                // Force le refresh de l'ArrayList des Tables du service actuel.
                Tables.initialize(idService);

                // Appel de la méthode de changement de vue.
                switchView();
            } else {
                textViewErreur.setText("Champs incorrect.");
            }
        });

        /** Listener du boutton Supprimer */
        buttonSupprimer.setOnClickListener(v -> {
            // Envoie de la requête à l'API.
            TableDAO.deleteTable(targetedTable);

            // Force le refresh de l'ArrayList des Tables du service actuel.
            Tables.initialize(idService);

            // Appel de la méthode de changement de vue.
            switchView();
        });

    }


    /**
     * Méthode qui remplie les champs avec les infos de la table.
     * @param targetedTable Table
     * @param textViewNumTable TextView
     * @param editTextNbPlace EditText
     * @param spinnerServeurAffectee Spinner
     */
    private void fillFields(Table targetedTable, TextView textViewNumTable, EditText editTextNbPlace, Spinner spinnerServeurAffectee) {
        // Remplies les editTexts.
        textViewNumTable.setText("Table n° " + targetedTable.getNumTable());
        editTextNbPlace.setText(String.valueOf(targetedTable.getNbPlace()));

        // Récupère l'ArrayList des prenoms+noms des serveurs et ajoute une valeur désignant l'absence d'affectation.
        ArrayList<String> serveursNames = Serveurs.getServeursNames();
        serveursNames.add(0, "Pas d'affectation");

        // DropDown menu sélection du serveur affectée.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, serveursNames);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerServeurAffectee.setAdapter(adapter);
        spinnerServeurAffectee.setOnItemSelectedListener(this);
    }


    /**
     * Méthode qui change de vue.
     */
    private void switchView() {
        Intent intent = new Intent(ModifTable.this, chef_salle.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {}

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}
}