package com.example.ap_android_amphitryon.modeles.DTO;


import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Serveurs;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Table implements Serializable {

    private int numTable;
    private LocalDate dateJour;
    private int idService;
    private int idUser;
    private int nbPlace;


    public Table () {
    }
    public Table(int numTable, LocalDate dateJour, int idService, int idUser, int nbPlace) {
        this.numTable = numTable;
        this.dateJour = dateJour;
        this.idService = idService;
        this.idUser = idUser;
        this.nbPlace = nbPlace;
    }


    public int getNumTable() {
        return this.numTable;
    }

    public void setNumTable(int numTable) {
        this.numTable = numTable;
    }

    public LocalDate getDateJour() {
        return this.dateJour;
    }

    public String getDateJourString() {
        return this.dateJour.toString();
    }

    public void setDateJour(String dateJour) {
        LocalDate correctDateJour = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            correctDateJour = LocalDate.parse(dateJour, formatter);
        }

        this.dateJour = correctDateJour;
    }

    public int getIdService() {
        return this.idService;
    }

    public void setIdService(int idService) {
        this.idService = idService;
    }

    public int getIdUser() {
        return this.idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getNbPlace() {
        return this.nbPlace;
    }

    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }


    /**
     * Méthode retournant le texte à afficher dans le linearLayout.
     * @return string
     */
    public String getLabel() {
        return "Table n°" + this.numTable;
    }


    /**
     * Méthode retournant le texte à afficher dans le linearLayout.
     * @return string
     */
    public String getInfos() {
        String str = "";

        if (this.idUser == 0) {
            str += "Pas affectée,  ";
        } else {
            Utilisateur serveur = Serveurs.getServeurById(this.idUser);
            str += serveur.getNom() + " " + serveur.getPrenom() + ",  ";
        }

        str += this.nbPlace + " places";

        return str;
    }
}
