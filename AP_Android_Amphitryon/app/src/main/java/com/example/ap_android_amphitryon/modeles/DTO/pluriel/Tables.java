package com.example.ap_android_amphitryon.modeles.DTO.pluriel;

import com.example.ap_android_amphitryon.modeles.DAO.TableDAO;
import com.example.ap_android_amphitryon.modeles.DTO.Table;

import java.util.ArrayList;

public class Tables {

    /**
     * ArrayLists<Table> contenant les objets Table du service du midi
     * et du service du soir d'aujourd'hui.
     */
    private static ArrayList<Table> lesTablesMidi;
    private static ArrayList<Table> lesTablesSoir;


    /**
     * Méthode initialisant l'ArrayList<Table> lesTables.
     */
    public static void initialize(int idService) {
        if (idService == 1) {
            lesTablesMidi = TableDAO.getTables(idService);
        } else if (idService == 2) {
            lesTablesSoir = TableDAO.getTables(idService);
        }
    }



    /**
     * Méthode qui retourne une ArrayList des tables d'un service d'aujourd'hui.
     * @param idService
     * @return
     */
    public static ArrayList<Table> getTables(int idService) {
        if (idService == 1) {
            if (lesTablesMidi == null) {
                initialize(idService);
            }
            return lesTablesMidi;
        } else if (idService == 2) {
            if (lesTablesSoir == null) {
                initialize(idService);
            }
            return lesTablesSoir;
        }
        return null;
    }



    /**
     * Méthode qui renvoie le plus grand numéro de table d'un service d'aujourd'hui.
     * @param idService
     * @return
     */
    public static int getMaxNumTable(int idService) {
        ArrayList<Table> lesTables;
        if (idService == 1) {
            lesTables = lesTablesMidi;
        } else {
            lesTables = lesTablesSoir;
        }

        int maxNumTable = 1;
        if (lesTables.size() == 0) {
            return 0;
        }
        for (Table uneTable: lesTables) {
            if (uneTable.getNumTable() > maxNumTable) {
                maxNumTable = uneTable.getNumTable();
            }
        }

        return maxNumTable;
    }
}