-- MySQL dump 10.13  Distrib 8.0.33, for Linux (x86_64)
--
-- Host: localhost    Database: amphitryon
-- ------------------------------------------------------
-- Server version	8.0.33-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commande` (
  `idCommande` int NOT NULL AUTO_INCREMENT,
  `idService` int NOT NULL,
  `numTable` int NOT NULL,
  `dateJour` date NOT NULL,
  `heureCommande` time DEFAULT NULL,
  `etatCommande` char(100) DEFAULT NULL,
  PRIMARY KEY (`idCommande`),
  KEY `I_FK_commande_tableJour` (`numTable`,`dateJour`,`idService`),
  CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`numTable`, `dateJour`, `idService`) REFERENCES `tableJour` (`numTable`, `dateJour`, `idService`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commande`
--

LOCK TABLES `commande` WRITE;
/*!40000 ALTER TABLE `commande` DISABLE KEYS */;
INSERT INTO `commande` VALUES (1,2,1,'2023-04-04','12:07:15','réglée'),(2,2,2,'2023-04-04','15:10:01','non-réglée'),(3,2,3,'2023-04-04','14:11:32','réglée');
/*!40000 ALTER TABLE `commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `composer`
--

DROP TABLE IF EXISTS `composer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `composer` (
  `idCommande` int NOT NULL,
  `idPlat` int NOT NULL,
  `commentaire` char(255) DEFAULT NULL,
  `etatPlat` char(100) DEFAULT NULL,
  `quantite` int DEFAULT NULL,
  PRIMARY KEY (`idCommande`,`idPlat`),
  KEY `I_FK_composer_commande` (`idCommande`),
  KEY `I_FK_composer_Plat` (`idPlat`),
  CONSTRAINT `composer_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`idCommande`),
  CONSTRAINT `composer_ibfk_2` FOREIGN KEY (`idPlat`) REFERENCES `plat` (`idPlat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `composer`
--

LOCK TABLES `composer` WRITE;
/*!40000 ALTER TABLE `composer` DISABLE KEYS */;
INSERT INTO `composer` VALUES (1,2,'L’œuf doit être bien cuit','commandé',4),(2,3,'Le poulet doit être saignant.','servi',3),(3,4,'J\'aimerais que la tomate sois bien rouge et juteuse hmmmmmmmm.','desservi',6);
/*!40000 ALTER TABLE `composer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plat`
--

DROP TABLE IF EXISTS `plat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plat` (
  `idPlat` int NOT NULL AUTO_INCREMENT,
  `codeType` char(10) NOT NULL,
  `nomPlat` char(100) DEFAULT NULL,
  `descriptionPlat` char(255) DEFAULT NULL,
  PRIMARY KEY (`idPlat`),
  KEY `I_FK_plat_typePlat` (`codeType`),
  CONSTRAINT `plat_ibfk_1` FOREIGN KEY (`codeType`) REFERENCES `typePlat` (`codeType`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plat`
--

LOCK TABLES `plat` WRITE;
/*!40000 ALTER TABLE `plat` DISABLE KEYS */;
INSERT INTO `plat` VALUES (1,'1','Gougères au fromage','La gougère est une spécialité bourguignonne, et plus particulièrement icaunaise (originaire des environs d\'Auxerre), composée de pâte à choux et de fromage, le fromage étant mélangé à la pâte à choux encore tiède avant la cuisson.'),(2,'1','Oeuf cocotte','L’œuf en cocotte (ou œuf cocotte) est un œuf cassé dans une petite cocotte ou un ramequin, préalablement beurré ou apprêtés de divers appareils ou garnitures, comme des foies de canard (à la rouennaise) ou encore des quenelles de volaille truffée (à la Bé'),(3,'2','Poulet Basquaise','Le poulet basquaise ou poulet à la basquaise est une spécialité culinaire de cuisine traditionnelle emblématique de la cuisine basque, étendue avec le temps à la cuisine française, à base de morceaux de poulet mijotés dans une piperade.'),(4,'2','Tomates farcies','Les tomates farcies sont une spécialité culinaire traditionnelle des cuisines du bassin méditerranéen français. Ce plat est composé de riz, de farce à la viande, de tomates et d\'herbes fraîches mais il est également possible de remplacer la farce à la via'),(5,'2','Croque-monsieur traditionnel','Un croque-monsieur ou croquemonsieur est un sandwich chaud, originaire de France, à base de pain, de jambon blanc et de fromage.'),(6,'3','Riz au lait','Le riz au lait est une spécialité culinaire sucrée composée au moins de riz cuit dans du lait puis sucré. Il est souvent aromatisé à la cannelle ou à la vanille.'),(7,'3','Éclair au chocolat','L\'éclair au chocolat est une pâtisserie d\'origine française constituée de pâte à choux allongée et fourrée de crème au chocolat , avec un glaçage sur le dessus.');
/*!40000 ALTER TABLE `plat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proposerPlat`
--

DROP TABLE IF EXISTS `proposerPlat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proposerPlat` (
  `idPlat` int NOT NULL,
  `idService` int NOT NULL,
  `dateJour` date NOT NULL,
  `quantiteProposer` int DEFAULT NULL,
  `prixVente` int DEFAULT NULL,
  `prixVendue` int DEFAULT NULL,
  PRIMARY KEY (`idPlat`,`idService`,`dateJour`),
  KEY `I_FK_proposerPlat_Plat` (`idPlat`),
  KEY `I_FK_proposerPlat_Service` (`idService`),
  CONSTRAINT `proposerPlat_ibfk_1` FOREIGN KEY (`idPlat`) REFERENCES `plat` (`idPlat`),
  CONSTRAINT `proposerPlat_ibfk_2` FOREIGN KEY (`idService`) REFERENCES `service` (`idService`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proposerPlat`
--

LOCK TABLES `proposerPlat` WRITE;
/*!40000 ALTER TABLE `proposerPlat` DISABLE KEYS */;
INSERT INTO `proposerPlat` VALUES (1,1,'2023-04-07',100,30,50),(2,2,'2023-04-08',30,10,20),(3,1,'2023-04-07',80,40,40);
/*!40000 ALTER TABLE `proposerPlat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `service` (
  `idService` int NOT NULL,
  `libelleService` char(100) DEFAULT NULL,
  PRIMARY KEY (`idService`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Midi'),(2,'Soir');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tableJour`
--

DROP TABLE IF EXISTS `tableJour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tableJour` (
  `numTable` int NOT NULL,
  `dateJour` date NOT NULL,
  `idService` int NOT NULL,
  `idUser` char(32) DEFAULT NULL,
  `nbPlace` int DEFAULT NULL,
  PRIMARY KEY (`numTable`,`dateJour`,`idService`),
  KEY `I_FK_tableJour_Service` (`idService`),
  KEY `I_FK_tableJour_Utilisateur` (`idUser`),
  CONSTRAINT `tableJour_ibfk_1` FOREIGN KEY (`idService`) REFERENCES `service` (`idService`),
  CONSTRAINT `tableJour_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tableJour`
--

LOCK TABLES `tableJour` WRITE;
/*!40000 ALTER TABLE `tableJour` DISABLE KEYS */;
INSERT INTO `tableJour` VALUES (1,'2023-04-04',2,'20',4),(1,'2023-04-10',1,'20',3),(1,'2023-04-11',2,'20',2),(1,'2023-04-14',1,'20',6),(1,'2023-04-14',2,'20',2),(1,'2023-04-17',1,'20',2),(1,'2023-05-02',1,'20',5),(2,'2023-04-04',2,'21',3),(2,'2023-04-10',1,'21',4),(2,'2023-04-14',1,'21',4),(2,'2023-04-14',2,'2',6),(2,'2023-04-17',1,NULL,4),(2,'2023-05-02',1,'2',4),(3,'2023-04-04',2,'20',6),(3,'2023-04-14',1,'2',7),(4,'2023-04-14',1,NULL,4);
/*!40000 ALTER TABLE `tableJour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typePlat`
--

DROP TABLE IF EXISTS `typePlat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `typePlat` (
  `codeType` char(10) NOT NULL,
  `libelleType` char(100) DEFAULT NULL,
  PRIMARY KEY (`codeType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typePlat`
--

LOCK TABLES `typePlat` WRITE;
/*!40000 ALTER TABLE `typePlat` DISABLE KEYS */;
INSERT INTO `typePlat` VALUES ('1','entrée'),('2','plat principal'),('3','dessert');
/*!40000 ALTER TABLE `typePlat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utilisateur` (
  `id` char(32) NOT NULL,
  `nom` char(100) DEFAULT NULL,
  `prenom` char(100) DEFAULT NULL,
  `login` char(100) DEFAULT NULL,
  `mdp` char(100) DEFAULT NULL,
  `statut` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES ('1','BOUTEVIN SANCE','Lucas','Eren','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','ChefDeCuisine'),('10','nomuser','prenomuser','guigui','a39f0a6e0b5e26f8e6ee8ccebb62e21024cb78c907b30720dd18e45423a56038','ChefDeSalle'),('2','AKAR','Kamel','serveur','a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3','Serveur'),('20','Sarah','Croche','sarah','d233633d9524e84c71d6fe45eb3836f8919148e4a5fc2234cc9e6494ec0f11c2','Serveur'),('21','Cecile','Ouinkça','cecile','a79dea865e52486df837e202c48b9b2c8805df8ae3700b695458dfe73a55d752','Serveur');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-19  9:45:27
