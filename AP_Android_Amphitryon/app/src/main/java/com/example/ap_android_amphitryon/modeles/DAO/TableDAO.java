package com.example.ap_android_amphitryon.modeles.DAO;

import com.example.ap_android_amphitryon.lib.HttpRequest;
import com.example.ap_android_amphitryon.modeles.DTO.Table;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;


public class TableDAO {

    /**
     * Méthode qui retourne la date du jour en chaine de caractère.
     * @return String
     */
    private static String getDateString() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateTime = LocalDateTime.now();

        return localDateTime.format(dateTimeFormatter);
    }



    /**
     * Méthode retournant toute les tables de la BDD.
     * @return ArrayList<Table>
     */
    public static ArrayList<Table> getTables(int idService) {
        RequestBody formBody = new FormBody.Builder()
                .add("dateJour", getDateString())
                .add("idService", String.valueOf(idService))
                .build();

        String responseBody = HttpRequest.executeRequest("table/getTables.php", "POST", formBody);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(responseBody, new TypeReference<ArrayList<Table>>(){});
        } catch (JsonProcessingException e) {
            System.out.println("Error parsing json to Table object");
        }

        return null;
    }




    /**
     * Méthode qui modifie une table.
     * @param table Table
     */
    public static String editTable(Table table) {
        RequestBody formBody = new FormBody.Builder()
                .add("numTable", String.valueOf(table.getNumTable()))
                .add("idService", String.valueOf(table.getIdService()))
                .add("dateJour", table.getDateJourString())
                .add("idUser", String.valueOf(table.getIdUser()))
                .add("nbPlace", String.valueOf(table.getNbPlace()))
                .build();

        return HttpRequest.executeRequest("table/editTable.php", "POST", formBody);
    }


    /**
     * Méthode qui supprime une table.
     * @param targetedTable Table
     */
    public static String deleteTable(Table targetedTable) {
        RequestBody formBody = new FormBody.Builder()
                .add("numTable", String.valueOf(targetedTable.getNumTable()))
                .add("idService", String.valueOf(targetedTable.getIdService()))
                .add("dateJour", targetedTable.getDateJourString())
                .build();

        return HttpRequest.executeRequest("table/deleteTable.php", "POST", formBody);
    }


    /**
     * Méthode qui ajoute une table.
     * @param table Table
     */
    public static String addTable(Table table) {
        RequestBody formBody = new FormBody.Builder()
                .add("numTable", String.valueOf(table.getNumTable()))
                .add("dateJour", table.getDateJourString())
                .add("idService", String.valueOf(table.getIdService()))
                .add("idUser", String.valueOf(table.getIdUser()))
                .add("nbPlace", String.valueOf(table.getNbPlace()))
                .build();

        return HttpRequest.executeRequest("table/addTable.php", "POST", formBody);
    }
}
