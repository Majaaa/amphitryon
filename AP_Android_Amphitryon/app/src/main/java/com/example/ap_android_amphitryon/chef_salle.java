package com.example.ap_android_amphitryon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ap_android_amphitryon.lib.BackButton;
import com.example.ap_android_amphitryon.lib.GenerateXML;
import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Tables;
import com.example.ap_android_amphitryon.modeles.DTO.Table;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;


public class chef_salle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_salle);

        // Récupère le boutton retour, et met un listener dessus.
        FloatingActionButton backButton = findViewById(R.id.backButton);
        BackButton.listenerOnBackButton(backButton, chef_salle.this, MainActivity.class);

        // Récupère le boutton créer table.
        Button buttonCreerTable = findViewById(R.id.buttonCreerTable);

        // Récupère le RadioGroup permettant de sélectionner les tables à afficher en fonction du service.
        RadioGroup radioGroupService = findViewById(R.id.radioGroupService);

        // Récupère les RadioButton du radioGroup.
        RadioButton radioButtonMidi = findViewById(R.id.buttonRadioMidi);
        RadioButton radioButtonSoir = findViewById(R.id.buttonRadioSoir);

        // Appel de la méthode qui coche le buttonRadio par défaut en fonction de l'heure actuel.
        defaultCheckRadioGroup(radioGroupService);

        /** Listener sur le RadioGroup. */
        radioGroupService.setOnCheckedChangeListener((group, checkedId) -> {
            if (radioButtonMidi.isChecked()) {
                initializeLinearLayout(Tables.getTables(1));
            } else if (radioButtonSoir.isChecked()) {
                initializeLinearLayout(Tables.getTables(2));
            }
        });

        /** Listener sur le bouton créer table. */
        buttonCreerTable.setOnClickListener(v -> {
            Intent intent = new Intent(chef_salle.this, AjoutTable.class);
            startActivity(intent);
        });

        // Initialise le LinearLayout et les listeners.
        initializeLinearLayout(Tables.getTables(getIdServiceByDate()));
    }


    /**
     * Méthode qui: - Récupère le LinearLayout de la vue.
     *              - Vide le contenue actuel du linearLayout.
     *              - Remplie le linearLayout avec les Tables souhaité (Méthode de GenerateXML).
     *              - Génère les listeners pour les textViews (Méthode de GenerateXML).
     * @param lesTables ArrayList<Table>
     */
    private void initializeLinearLayout(ArrayList<Table> lesTables) {
        LinearLayout linearLayoutTables = findViewById(R.id.linearLayoutTables);
        linearLayoutTables.removeAllViews();

        HashMap<Integer, Object> hashMapId = GenerateXML.generateTextViews(lesTables, linearLayoutTables);

        GenerateXML.onClickChangeIntent(hashMapId, chef_salle.this, ModifTable.class);
    }


    /**
     * Méthode qui coche le radioButton par défaut en fonction de l'heure de la journée.
     * (Si il n'est pas encore 16h -> Midi, sinon Soir.)
     * @param radioGroupService RadioGroup
     */
    private void defaultCheckRadioGroup(RadioGroup radioGroupService) {
        if (getHourOfDay() < 16) {
            radioGroupService.check(R.id.buttonRadioMidi);
        } else {
            radioGroupService.check(R.id.buttonRadioSoir);
        }
    }


    /**
     * Méthode qui retourne l'heure du jour.
     * @return int
     */
    private static int getHourOfDay() {
        return LocalDateTime.now().getHour();
    }


    /**
     * Méthode qui retourne l'id du service en fonction de l'heure actuel.
     * @return int
     */
    private static int getIdServiceByDate() {
        if (getHourOfDay() < 16) {
            return 1;
        }
        return 2;
    }
}
