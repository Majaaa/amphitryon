<?php

require_once '../../modeles/DAO/Param.php';
require_once '../../modeles/DAO/DBConnex.php';
require_once '../../modeles/DAO/TableDAO.php';

if ($_POST['numTable'] <= TableDAO::getMaxNumTable($_POST['idService'], $_POST['dateJour'])) {
    TableDAO::incrementNextTables($_POST['numTable'], $_POST['idService'], $_POST['dateJour']);
}

print(json_encode(TableDAO::addTable($_POST['numTable'], $_POST['dateJour'], $_POST['idService'], $_POST['idUser'], $_POST['nbPlace'])));

?>