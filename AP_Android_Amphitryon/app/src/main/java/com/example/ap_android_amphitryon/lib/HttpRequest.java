package com.example.ap_android_amphitryon.lib;

import java.util.concurrent.atomic.AtomicReference;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpRequest {

    // Adresse de l'API.
    //private final static String api_url = "http://10.100.0.5/~lboutevin/API_a_utiliser/controleurs/";
    private final static String api_url = "http://10.0.2.2:80/controleurs/";
//    private final static String api_url = "http://10.0.2.2:80/API_a_utiliser/controleurs/";
//    private final static String api_url = "http://10.0.2.2:80/API/controleurs/";


    /**
     * Méthode qui execute une requête et renvoie la réponse.
     * @param endPoint (String : endPoint vers l'API.)
     * @param requestType (String : Type de la requête)
     * @param requestBody (Objet RequestBody)
     * @return String (Réponse de la requête)
     */
    public static String executeRequest(String endPoint, String requestType, RequestBody requestBody) {
        // Déclaration & instantiation d'un objet OkHttpClient pour la requête à la BDD.
        OkHttpClient client = new OkHttpClient();

        // Création de la requête
        // Met l'URL vers l'endpoint d'authentification de l'API
        Request.Builder requestBuilder = new Request.Builder()
                .url(api_url + endPoint);

        // Met le type de la requête
        if (requestType.equals("POST")) {
            requestBuilder.post(requestBody);

        } else if(requestType.equals("GET")){
            requestBuilder.get();
        }

        // Prépare la requête
        Request request = requestBuilder.build();

        // Déclaration de la chaine de caractère qui contiendra la réponse de la requête.
        AtomicReference<String> responseBody = new AtomicReference<>("");

        // Execution de la requête dans un Thread.
        Thread thread = new Thread(() -> {
            // Utilisation d'un try/catch en cas d'erreur de connexion à la BDD.
            try {
                // Exécution de la requête & réponse de la requête dans responseBody.
                Response response = client.newCall(request).execute();
                System.out.println(response);
                responseBody.set(response.body().string());

            } catch (Exception e) {
                // Afficher l'erreur générique
                System.err.println("Exception: " + e.getMessage());
            }
        });

        // Execution du thread dans un try/catch.
        try {
            thread.start();
            thread.join();
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        }

        return responseBody.get();
    }

}


