package com.example.ap_android_amphitryon;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ap_android_amphitryon.lib.BackButton;
import com.example.ap_android_amphitryon.lib.InputControl;
import com.example.ap_android_amphitryon.modeles.DAO.TableDAO;
import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Serveurs;
import com.example.ap_android_amphitryon.modeles.DTO.pluriel.Tables;
import com.example.ap_android_amphitryon.modeles.DTO.Table;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class AjoutTable extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_table);

        // Déclaration le boutton retour, et met un listener dessus.
        FloatingActionButton backButton = findViewById(R.id.backButton);
        BackButton.listenerOnBackButton(backButton, AjoutTable.this, chef_salle.class);

        // Déclaration boutton création table.
        final Button buttonCreerAjout = findViewById(R.id.buttonCreerAjout);

        // Déclaration editTexts, textView et spinners
        EditText editTextNumTableAjout = findViewById(R.id.editTextNumTableAjout);
        EditText editTextNbPlaceAjout = findViewById(R.id.editTextNbPlaceAjout);
        Spinner spinnerServiceAjout = findViewById(R.id.spinnerServiceAjout);
        Spinner spinnerServeurAffecteeAjout = findViewById(R.id.spinnerServeurAffecteeAjout);
        TextView textViewErreurAjout = findViewById(R.id.textViewErreurAjout);


        // Appel de la méthode qui remplie les spinners.
        fillSpinners(spinnerServiceAjout, spinnerServeurAffecteeAjout);


        // Listener sur le bouton de création.
        buttonCreerAjout.setOnClickListener(v -> {
            // Récupère les valeurs du formulaire.
            int idService = (int) spinnerServiceAjout.getSelectedItemId() + 1;
            String numTable = editTextNumTableAjout.getText().toString();
            String nbPlace = editTextNbPlaceAjout.getText().toString();
            int idServeur;
            if (spinnerServeurAffecteeAjout.getSelectedItemId() != 0) {
                idServeur = Integer.parseInt(Serveurs.getServeurs().get((int) spinnerServeurAffecteeAjout.getSelectedItemId() - 1).getId());
            } else {
                idServeur = 0;
            }

            // Si les controles de saisies sont correct.
            if (InputControl.controlNumTable(numTable, idService) && InputControl.controlNbPlace(nbPlace)) {
                // Création d'un objet avec la nouvelle table.
                Table newTable = new Table(Integer.parseInt(numTable), LocalDate.now(), idService, idServeur, Integer.parseInt(nbPlace));

                // Envoie de la requête à l'API.
                TableDAO.addTable(newTable);

                // Force le refresh de l'ArrayList des Tables du service actuel.
                Tables.initialize(idService);


                // Retour à la page de visualisation des tables.
                Intent intent = new Intent(AjoutTable.this, chef_salle.class);
                startActivity(intent);
            } else {
                textViewErreurAjout.setText("Champs incorrect.");
            }
        });


    }

    /**
     * Méthode qui remplies les spinners.
     * @param spinnerServiceAjout Spinner
     * @param spinnerServeurAffecteeAjout Spinner
     */
    private void fillSpinners(Spinner spinnerServiceAjout, Spinner spinnerServeurAffecteeAjout) {
        // Tableau contenant les deux services à choisir.
        String[] serviceLibelles = new String[] {"Midi", "Soir"};

        // Spinner sélection du service.
        ArrayAdapter<String> adapterService = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, serviceLibelles);

        adapterService.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerServiceAjout.setAdapter(adapterService);
        spinnerServiceAjout.setOnItemSelectedListener(this);
        spinnerServiceAjout.setSelection(getDefaultServiceId()-1);

        // Spinner sélection du serveur affectée.

        // Récupère l'ArrayList des prenoms+noms des serveurs et ajoute une valeur désignant l'absence d'affectation.
        ArrayList<String> serveursNames = Serveurs.getServeursNames();
        serveursNames.add(0, "Pas d'affectation");

        ArrayAdapter<String> adapterServeur = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, serveursNames);

        adapterServeur.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerServeurAffecteeAjout.setAdapter(adapterServeur);
        spinnerServeurAffecteeAjout.setOnItemSelectedListener(this);

    }



    /**
     * Méthode qui renvoie l'id de service à selectionner par défaut en fonction de l'heure du jour.
     * @return int
     */
    private static int getDefaultServiceId() {
        if (LocalDateTime.now().getHour() < 16) {
            return 1;
        }
        return 2;
    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {}

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {}

}