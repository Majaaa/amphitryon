<?php

class TableDAO {

    /**
     * Méthode qui retourne toutes les tables d'un service d'aujourd'hui.
     * @param String idService
     * @param String dateJour
     * @return String
     */
    public static function getTables(String $idService, String $dateJour) {
        try {
            $prepSql = "SELECT *
                        FROM tableJour
                        WHERE idService = :idService
                        AND dateJour = :dateJour;";
            
            $sql = DBConnex::getInstance()->prepare($prepSql);
            $sql->bindParam("idService", $idService);
            $sql->bindParam("dateJour", $dateJour);
			$sql->execute();

            return $sql->fetchAll(PDO::FETCH_ASSOC);

        } catch(Exception $e) {}

        return "Failed";
    }


    /** 
    * Méthode qui retourne la plus grand numTable d'une servie à une date.
    * @param String idService  
    * @param String dateJour
    * @return String
    */ 
    public static function getMaxNumTable(String $idService, String $dateJour) {
        $sqlGetMaxNumTable = "SELECT MAX(numTable)
                              FROM tableJour
                              WHERE idService = :idService
                              AND dateJour = :dateJour;";

        $sql = DBConnex::getInstance()->prepare($sqlGetMaxNumTable);
        $sql->bindParam("idService", $idService);
        $sql->bindParam("dateJour", $dateJour);
        $sql->execute();

        return $sql->fetch(PDO::FETCH_NUM)[0];
    }


    /**
     * Méthode qui incrémente toute les tables dont le numéro table est supérieur ou égale à celui passé en paramètre
     * et dont la date et le numéro de service sont ceux passé en paramètre.
     * @param String numTable
     * @param String dateJour
     * @param String idService
     * @return String
     */
    public static function incrementNextTables(String $numTable, String $idService, String $dateJour) {
        try {
            $maxNumTable = TableDAO::getMaxNumTable($idService, $dateJour);


            /** numTable + maxNumTable dont le numTable est supérieur ou égale à celui passé en paramètre */
            $numTablePlusMaxNumTable = "UPDATE tableJour
                                        SET numTable = numTable + :maxNumTable
                                        WHERE numTable >= :numTable
                                        AND idService = :idService
                                        AND dateJour = :dateJour;";
            
            $sql = DBConnex::getInstance()->prepare($numTablePlusMaxNumTable);
            $sql->bindParam("maxNumTable", $maxNumTable);
            $sql->bindParam("numTable", $numTable);
            $sql->bindParam("idService", $idService);
            $sql->bindParam("dateJour", $dateJour);
            $sql->execute();


            /** numTable - maxNumTable + 1 dont le numTable est supérieur ou égale à celui passé en paramètre + maxNumTable */
            $numTableMinusMaxNumTablePlusOne = "UPDATE tableJour
                                                SET numTable = numTable - :maxNumTable + 1
                                                WHERE numTable >= :numTable + :maxNumTable
                                                AND idService = :idService
                                                AND dateJour = :dateJour;";
            
            $sql = DBConnex::getInstance()->prepare($numTableMinusMaxNumTablePlusOne);
            $sql->bindParam("maxNumTable", $maxNumTable);
            $sql->bindParam("numTable", $numTable);
            $sql->bindParam("idService", $idService);
            $sql->bindParam("dateJour", $dateJour);
            $sql->execute();

            return $sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {}
        
        return "Failed";
    }



    /**
     * Méthode qui modifie une table dans la BDD.
     * @param String numTable
     * @param String newNumTable
     * @param String idService
     * @param String idUser
     * @param String nbPlace
     * @param String dateJour
     * @return String
     */
    public static function editTable(String $numTable, String $idService, String $idUser, String $nbPlace, String $dateJour) {
        try {

            // Si aucun serveur n'a été affectée à la table.
            if ($idUser == "0") {
                $prepSql = "UPDATE tableJour    
                                SET idUser = NULL,
                                    nbPlace = :nbPlace
                            WHERE numTable = :numTable
                            AND idService = :idService
                            AND dateJour = :dateJour;";
            
                $sql = DBConnex::getInstance()->prepare($prepSql);

                $sql->bindParam("nbPlace", $nbPlace);
                $sql->bindParam("numTable", $numTable);
                $sql->bindParam("idService", $idService);
                $sql->bindParam("dateJour", $dateJour);
                $sql->execute();
                
                // Sinon (Si un serveur a été affectée.)
            } else {
                $prepSql = "UPDATE tableJour    
                                SET idUser = :idUser,
                                    nbPlace = :nbPlace
                            WHERE numTable = :numTable
                            AND idService = :idService
                            AND dateJour = :dateJour;";
            
                $sql = DBConnex::getInstance()->prepare($prepSql);

                $sql->bindParam("idUser", $idUser);
                $sql->bindParam("nbPlace", $nbPlace);
                $sql->bindParam("numTable", $numTable);
                $sql->bindParam("idService", $idService);
                $sql->bindParam("dateJour", $dateJour);

                $sql->execute();
            }

            return $sql->fetchAll(PDO::FETCH_ASSOC);
        } catch(Exception $e) {}

        return "Failed";
    }


    /**
     * Méthode qui supprime une table d'un service d'un jour.
     * @param String numTable
     * @param String idService
     * @param String dateJour
     * @return String
     */
    public static function deleteTable(String $numTable, String $idService, String $dateJour) {
        try {
            $prepSql = "DELETE FROM tableJour
                        WHERE numTable = :numTable
                        AND idService = :idService
                        AND dateJour = :dateJour;";

            $sql = DBConnex::getInstance()->prepare($prepSql);
            $sql->bindParam("numTable", $numTable);
            $sql->bindParam("idService", $idService);
            $sql->bindParam("dateJour", $dateJour);
            $sql->execute();

            return $sql->fetchAll(PDO::FETCH_ASSOC);
        }catch (Exception $e) {}

        return "Failed";
    }


    /**
     * Méthode qui ajoute une table à un service à un jour.
     * @param String numTable 
     * @param String dateJour
     * @param String idService
     * @param String idUser
     * @param String nbPlace
     * @return String
     */
    public static function addTable(String $numTable, String $dateJour, String $idService, String $idUser, String $nbPlace) {
        try {
            // Si aucun serveur n'a été affectée à la table.
            if ($idUser == "0") {
                $prepSql = "INSERT INTO tableJour (numTable, dateJour, idService, idUser, nbPlace)
                            VALUES (:numTable, :dateJour, :idService, NULL, :nbPlace);";

                $sql = DBConnex::getInstance()->prepare($prepSql);
                $sql->bindParam("numTable", $numTable);
                $sql->bindParam("dateJour", $dateJour); 
                $sql->bindParam("idService", $idService); 
                $sql->bindParam("nbPlace", $nbPlace); 
                $sql->execute();

                // Sinon (Si un serveur a été affectée).
            } else {
                $prepSql = "INSERT INTO tableJour (numTable, dateJour, idService, idUser, nbPlace)
                            VALUES (:numTable, :dateJour, :idService, :idUser, :nbPlace);";

                $sql = DBConnex::getInstance()->prepare($prepSql);
                $sql->bindParam("numTable", $numTable);
                $sql->bindParam("dateJour", $dateJour); 
                $sql->bindParam("idService", $idService); 
                $sql->bindParam("idUser", $idUser); 
                $sql->bindParam("nbPlace", $nbPlace); 
                $sql->execute();
            }
            
            return $sql->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {}

        return "Failed";
    }
}

?>