package com.example.ap_android_amphitryon.modeles.DAO;

import com.example.ap_android_amphitryon.lib.HttpRequest;
import com.example.ap_android_amphitryon.modeles.DTO.Utilisateur;
import com.example.ap_android_amphitryon.modeles.Param;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.RequestBody;

public class UtilisateurDAO {

    /**
     * Méthode d'authentification d'un utilisateur.
     * @param login String
     * @param password String
     * @return boolean
     */
    public static boolean authentification(String login, String password) {
        // Chiffre le mdp en sha256.
        String sha256password = DigestUtils.sha256Hex(password);

        // Création du body de la requête.
        RequestBody formBody = new FormBody.Builder()
                .add("login", login)
                .add("mdp", sha256password)
                .build();

        // Appel de la méthode executent la requête.
        String responseBody = HttpRequest.executeRequest("utilisateur/Authentification.php", "POST", formBody);

        // Si le couple login/mdp est correct.
        if (responseBody.compareTo("false") != 0) {
            try {
                // Utilisation de Jackson pour créer un objet Utilisateur grâce au JSON de la réponse de la requête (JSON -> Objet).
                ObjectMapper objectMapper = new ObjectMapper();
                Utilisateur utilisateur = objectMapper.readValue(responseBody, Utilisateur.class);

                // L'utilisateur qui vient de se connecter est stocké dans la classe Param.
                Param.setCurrentUtilisateur(utilisateur);
            } catch (JsonProcessingException e) {
                System.out.println("Error while parsing user.");
            }

            return true;
        }

        return false;
    }


    /**
     * Méthode retournant tous les Utilisateurs dont le statut est `Serveur` de la BDD.
     * @return ArrayList<Utilisateur>
     */
    public static ArrayList<Utilisateur> getServeurs() {
        String responseBody = HttpRequest.executeRequest("utilisateur/getServeurs.php", "GET", null);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(responseBody, new TypeReference<ArrayList<Utilisateur>>(){});
        } catch (JsonProcessingException e) {
            System.out.println("Error parsing json to Utilisateur object");
        }

        return null;
    }


}